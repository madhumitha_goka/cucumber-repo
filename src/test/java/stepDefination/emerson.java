package stepDefination;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ThreadGuard;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class emerson {
	WebDriver driver; 
	
	 @Before
	    public void setup(Scenario scenario) throws Throwable {
		 System.setProperty("webdriver.chrome.driver", "/Users/mgoka/Desktop/Screen shots/Drivers/chromedriver"); 
			driver = new ChromeDriver(); 
			
	    }
	
	 @After
	    public void tearDown(Scenario scenario) {
	   
	        if (driver != null) {
	            try {
	                driver.quit();
	            } catch (Exception e) {

	            }
	        }
	    }
	
	@Given("^I am Emerson User with valid Log In details$")
	public void i_am_emerson_user_with_valid_log_in_details() throws Exception {
		 driver.get("https://www.emersonecologics.com");
		 Long loadtime = (Long)((JavascriptExecutor)driver).executeScript(
				    "return performance.timing.loadEventEnd - performance.timing.navigationStart;");
		 System.out.println(loadtime);
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 WebElement LogInButton = driver.findElement(By.xpath("//*[@class='emersonicon-class ng-scope'] [contains(text(),'Log In/Register')]")) ;
		 highlightElement(LogInButton);
		 LogInButton.click();
		 WebElement EmailId = driver.findElement(By.xpath("//*[@class='mdl-textfield__input'] [@id='EmailAddress'] ")) ;
		 highlightElement(EmailId);
		 EmailId.sendKeys("mgoka14@gmail.com");
		 WebElement PassWord = driver.findElement(By.xpath("//*[@class='mdl-textfield__input'] [@id='Password'] ")) ;
		 highlightElement(PassWord);
		 PassWord.sendKeys("Chaithu@14");
		 WebElement Submit = driver.findElement(By.xpath("//*[@class='mdl-button__ripple-container']")) ;
		 highlightElement(Submit);
		 Submit.click();
	 }
	 @When("^I navigate to emerson Homepage$")
	 public void i_navigate_to_emerson_Homepage() throws Exception {
		String homepageurl= driver.getCurrentUrl().trim();
		System.out.println(homepageurl);
	     
	 }
	 
	 @Given("^I navigate to emerson loginpage$")
		public void i_navigate_to_emerson_loginpage() throws Exception {
			 driver.get("https://www.emersonecologics.com");
			 WebElement LogInButton = driver.findElement(By.xpath("//*[@class='emersonicon-class ng-scope'] [contains(text(),'Log In/Register')]")) ;
			 highlightElement(LogInButton);
			 LogInButton.click();
			
		 }
	 
	 @When("^I entered correct emailid$")
	 public void i_entered_correct_emailid() throws Exception {
		 WebElement EmailId = driver.findElement(By.xpath("//*[@class='mdl-textfield__input'] [@id='EmailAddress'] ")) ;
		 highlightElement(EmailId);
		 EmailId.sendKeys("mgoka14@gmail.com");
	     
	 }
	 
	 @And("^I entered wrong password$")
	 public void i_entered_wrong_password() throws Exception { 
		 WebElement PassWord = driver.findElement(By.xpath("//*[@class='mdl-textfield__input'] [@id='Password'] ")) ;
		 highlightElement(PassWord);
		 PassWord.sendKeys("Cha@14");
		 WebElement Submit = driver.findElement(By.xpath("//*[@class='mdl-button__ripple-container']")) ;
		 highlightElement(Submit);
		 Submit.click();
	     
	 }
	 @And("^I should see error$")
	 public void i_should_see_error() throws Exception {
		 Thread.sleep(3000);
		 WebElement error = driver.findElement(By.xpath("//*[@class= 'error-container']")) ;
		 Thread.sleep(3000);
		 highlightElement(error);
		 Assert.assertNotNull(error);
	     
	 }
	 @And("^I should see shop Icon$")
	 public void i_should_see_shop_icon() throws Exception {
		 
		 WebElement shopelement = driver.findElement(By.xpath("//*[@class='text'] [contains(text(),'Shop')]")) ;
		 highlightElement(shopelement);
		String shopeelementexpected= shopelement.getText();
		System.out.println(shopeelementexpected);
		Assert.assertNotNull(shopelement);
		
	    
	 }
	 
	 @And("^I should mouse hover on shop Icon$")
	 public void i_should_mouse_hover_on_shop_icon() throws Exception {
		 WebElement shopelement = driver.findElement(By.xpath("//*[@class='text'] [contains(text(),'Shop')]")) ;
		 highlightElement(shopelement);
	    Thread.sleep(4000);
	    actionmethod(shopelement);
	    Thread.sleep(4000);
		   
	 }
	 

	 @Then("^I should see CATEGORY$")
	 public void i_should_see_CATEGORY() throws Exception {
		 WebElement Category = driver.findElement(By.xpath("//*[contains(text(),'CATEGORY')]")) ;
		 Thread.sleep(4000);
		 highlightElement(Category);
		 Thread.sleep(4000);
		 actionmethod(Category);
		 
		 Thread.sleep(4000);
		      
	 }
	 
	 @And("^I should see Additional Categories text$")
	 public void i_should_see_additional_categories_text() throws Exception {
		 WebElement addcatement = driver.findElement(By.xpath("//*[@class= 'addl-categories flex']")) ;
		 Thread.sleep(4000);
		 highlightElement(addcatement);
	    
	 }
	 

	 @Then("^I should see brand$")
	 public void i_should_see_brand() throws Exception {
		 WebElement brandele = driver.findElement(By.xpath("//*[contains(text(),'BRAND')]")) ;
		 Thread.sleep(4000);
		 highlightElement(brandele);
		 actionmethod(brandele);
		 Thread.sleep(4000);   	
	 }
	 
	 @And("^I should see Featured Brands text$")
	 public void i_should_see_featured_brands_text() throws Exception {
		 WebElement febrandelement = driver.findElement(By.xpath("//*[@class= 'header-title'][contains(text(),'Featured Brands')]")) ;
		 Thread.sleep(4000);
		 highlightElement(febrandelement);	   
	 }
	 
	 private void highlightElement(WebElement element) throws Exception {
			if(element.isDisplayed()){
				JavascriptExecutor js = (JavascriptExecutor) driver; 
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 2px solid red;");
				Thread.sleep(200);
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border:'';");
			}
		}
	 
	 private void actionmethod(WebElement element) throws Exception {
			if(element.isDisplayed()){
				Actions ac =  new Actions(driver);	
				 ac.moveToElement(element).perform();
			}
		}
	 
	
	
}
