#Author: your.email@your.domain.com
Feature: euroson

  @automationscenario
  Scenario: Validate shop Icon
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon

  @automationscenario 
  Scenario: Validate loginpage invalid password
    Given I navigate to emerson loginpage
    When I entered correct emailid
    And I entered wrong password
    And I should see error

  @automationscenario
  Scenario: Validate Catogery
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see CATEGORY
    And I should see Additional Categories text

   @automationscenario
  Scenario: Validate Brand
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see brand
    And I should see Featured Brands text

  @manual
  Scenario: Validate loginpage invalid email Id
    Given I navigate to emerson loginpage
    When I entered invalid emailid
    And I entered correct password
    And I should see error

  @manual
  Scenario: Validate loginpage invalid userdetails
    Given I navigate to emerson loginpage
    When I entered invalid emailid
    And I entered invalid password
    And I should see error

  @manual
  Scenario: Validate Nutritional Supplements link in Catagories
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see CATEGORY
    And I should see Nutritional Supplements link
    And I should be able to click on  Nutritional Supplements link

  @manual
  Scenario: Validate browse component in Brand
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see brand
    And I should see Browse text
    And I should be able to filter when I hit all button

  @manual
  Scenario: Validate various brands in Brand component
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see brand
    And I should see brand image
    And I shoudl see shop button
    Then I should be able hit shop button

  @manual
  Scenario: Validate
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see brand
    And I should see brand image
    And I shoudl see shop button
    Then I should be able hit shop button

  @manual
  Scenario: Validating mouse hover functionality on category
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see category
    When I take off mouse from table
    Then I should see homepage

  @manual
  Scenario: Validating mouse hover functionality on brand
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should see brand
    When I take off mouse from table
    Then I should see homepage

  @manual
  Scenario: Validating mouse hover functionality on brand
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    When I mouse hover on Brand
    Then I should see Brand text colour changed to black

  @manual
  Scenario: Validating mouse hover functionality on brand
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    When I mouse hover on category
    Then I should see category text colour changed to black

  @manual
  Scenario: Validating Browse functionality on brand page
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    When I mouse hover on brand
    Then I should click on A-G link
    And It is should take me to page where I can see filtered brands

  @manual
  Scenario: Validating links present on Category
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should mouse hover on Category
    And validate all links giving 200 response

  @manual
  Scenario: Validating links present on Category
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see shop Icon
    And I should mouse hover on shop Icon
    Then I should mouse hover on Category
    And validate all links giving 200 response

  @negativescenario
  Scenario: Validating brand is not present on Education
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see education link
    And I should mouse hover on education link
    Then I should not see brand

  @negativescenario
  Scenario: Validating category is not present on Education
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see education link
    And I should mouse hover on education link
    Then I should not see category

  @negativescenario
  Scenario: Validating category is not present on BRANDS
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see education link
    And I should mouse hover on education link
    Then I should not see category

  @negativescenario
  Scenario: Validating category is not present on QUALITY
    Given I am Emerson User with valid Log In details
    When I navigate to emerson Homepage
    And I should see education link
    And I should mouse hover on education link
    Then I should not see category or Brand
